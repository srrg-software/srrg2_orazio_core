#pragma once

uint8_t Orazio_sonarReady(void);
void Orazio_sonarInit(void);
void Orazio_sonarHandle(void);
